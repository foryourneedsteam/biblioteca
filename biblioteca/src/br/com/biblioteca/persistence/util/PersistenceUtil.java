package br.com.biblioteca.persistence.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/*
 * A classe PersistenceUtil sera utilizada no projeto com apenas uma instancia
 * Desse forma nao sera preciso criar diversos objetos dessa classe
 * Para isso o objeto entityManagerFactory foi declarado como static
 * O ojeto entityManagerFactory ira criar varios objetos EntityManager com uma unica instancia
 */
public class PersistenceUtil {
	
	private static EntityManagerFactory entityManagerFactory;
	
	/*
	 * Retorna uma única instância do EntityManagerFactory
	 * para todo projeto
	 */
	public static EntityManagerFactory getInstance(){
		/*
		 * Se a instancia do classe EntityManagerFactory cria uma nova
		 * Utilizando a Persistence Unit chamada bilioteca (presente no arquivo persistence.xml)
		 * E retorna a instancia da classe
		 */
		if(entityManagerFactory == null){
			entityManagerFactory = Persistence.createEntityManagerFactory("biblioteca");
		}
		return entityManagerFactory;
	}

}
