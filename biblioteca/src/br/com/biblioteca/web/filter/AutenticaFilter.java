package br.com.biblioteca.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter("/*")
public class AutenticaFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		System.out.println("AutenticaFilter called!");
		
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		String requestURI = httpServletRequest.getRequestURI();
		
		if (requestURI.equals("/biblioteca/login") || requestURI.equals("/biblioteca/autenticar")) {
			filterChain.doFilter(request, response);			
		} else {
			if (((HttpServletRequest) request).getSession().getAttribute("usuarioLogado") != null) {
				filterChain.doFilter(request, response);			
			} else {
				((HttpServletResponse) response).sendRedirect("/biblioteca/login");
			}
		}	
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
