package br.com.biblioteca.web.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.biblioteca.dao.UsuarioDAO;
import br.com.biblioteca.model.Usuario;

@Controller
public class LoginController {
	
	private UsuarioDAO usuarioDAO;
	
	/*
	 * Recebe a requisicao com mapeamento /login
	 * Redireciona para a pagina login.jsp na pasta raiz
	 * So ira utilizar o mapeamento se a requisicao for pelo metodo GET
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		return "login";
	}
	
	/*
	 * Metodo utilizado para iniciar o processo de autenticao do usuario
	 * Recebe a requisicao com mapeamento /autenticar
	 * Se autenticar o usuario, redireciona para o mapeamento /
	 * Se NAO autenticar o usuario, redireciona para o mapeamento /login com um parametro erro
	 * So ira utilizar o mapeamento se a requisicao for pelo metodo GET
	 */
	@RequestMapping(value = "/autenticar", method = RequestMethod.POST)
	public String autenticar(Usuario usuario, HttpSession session) {
		usuarioDAO = UsuarioDAO.getInstance();
		Usuario u = usuarioDAO.autenticarUsuario(usuario);
		
		if (u != null) {
			session.setAttribute("usuarioLogado", u);
			return "redirect:/";
		} else {
			return "redirect:/login?erro=invalido";
		}
	}
	
	/*
	 * Metodo utilizado para fazer logout na aplicacao
	 * Utiliza o metodo invalidate da classe Session para fazer logout
	 */
	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "/login";
	}
}
