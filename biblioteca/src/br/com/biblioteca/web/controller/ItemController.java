package br.com.biblioteca.web.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.biblioteca.dao.ItemDAO;
import br.com.biblioteca.model.Item;

@Controller
@RequestMapping("/item") // mapeia todas as requisicoes que comecem com /item
public class ItemController {
	
	/*
	 * Recebe a requisicao com mapeamento /item/novo
	 * Redireciona para a pagina novo.jsp na pasta item
	 * So ira utilizar o mapeamento se a requisicao for pelo metodo GET
	 */
	@RequestMapping(value = "/novo", method = RequestMethod.GET)
	public String novo() {
		return "/item/novo";		
	}
	
	/*
	 * Recebe a requisicao com mapeamento /item/salvar
	 * Inicia o tratamento para salvar um item no banco de dados
	 * So ira utilizar o mapeamento se a requisicao for pelo metodo POST
	 * @Valid valida o objeto item
	 * BindingResult permite verificar se algum erro de validacao foi encontrado no formulario
	 */
	@RequestMapping(value = "/salvar", method = RequestMethod.POST)
	public String salvar(@Valid Item item, BindingResult result) {
		// se algum erro for encontrado redireciona para a pagina do formulario
		if (result.hasErrors()) {
			return novo();
		}
		
		ItemDAO itemDAO = ItemDAO.getInstance(); // cria uma instancia da classe ItemDAO
		itemDAO.cadastraItem(item); // persiste o item no banco de dados
		
		return "/item/adicionado"; // redireciona para a pagina adicionada.jsp 
	}
	
	/*
	 * Recebe a requisicao com mapeamento /item/lista
	 * Inicia o tratamento para receber uma colecao de itens do banco de dados
	 * So ira utilizar o mapeamento se a requisicao for pelo metodo GET
	 */
	@RequestMapping(value = "/lista", method = RequestMethod.GET)
	public ModelAndView lista() {
		ItemDAO itemDAO = ItemDAO.getInstance(); // cria uma instancia da classe ItemDAO
		
		// cria um objeto da clase ModelAndView para adicionar parametros na pagina
		ModelAndView modelAndView = new ModelAndView("/item/lista");
		// adiciona a variavel items a colecao de itens do banco de dados
		modelAndView.addObject("items", itemDAO.getLista());
		
		return modelAndView;
	}
}
