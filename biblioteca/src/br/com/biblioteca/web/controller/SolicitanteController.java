package br.com.biblioteca.web.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.biblioteca.dao.SolicitanteDAO;
import br.com.biblioteca.model.Solicitante;
import br.com.biblioteca.model.Usuario;

@Controller
@RequestMapping("/solicitante") // mapeia todas as requisicoes que comecem com /solicitante
public class SolicitanteController {
	
	/*
	 * Recebe a requisicao com mapeamento /solicitante/novo
	 * Redireciona para a pagina novo.jsp na pasta solicitante
	 * So ira utilizar o mapeamento se a requisicao for pelo metodo GET
	 */
	@RequestMapping(value = "/novo", method = RequestMethod.GET)
	public String novo() {
		return "/solicitante/novo";
	}
	
	/*
	 * Recebe a requisicao com mapeamento /solicitante/salvar
	 * Inicia o tratamento para salvar um solicitante no banco de dados
	 * So ira utilizar o mapeamento se a requisicao for pelo metodo POST
	 * @Valid valida o objeto solicitante e usuario
	 * BindingResult permite verificar se algum erro de validacao foi encontrado no formulario
	 * IMPORTANTE: sempre manter um objeto do tipo BindingResult apos um objeto validado.
	 * Para esse caso foram 2 @Valid e 2 BindingResult (1 para cada classe a ser validada)
	 */
	@RequestMapping(value = "/salvar", method = RequestMethod.POST)
	public String salvar(@Valid Solicitante solicitante, BindingResult resultSolicitante,
			@Valid Usuario usuario, BindingResult resultUsuario) {
		// se algum erro for encontrado redireciona para a pagina do formulario
		if (resultSolicitante.hasErrors()) {
			return novo();
		}
		
		// se algum erro for encontrado redireciona para a pagina do formulario
		if (resultUsuario.hasErrors()) {
			return novo();
		}
		
		solicitante.setUsuario(usuario); // atribui o usuario ao solicitante 
		SolicitanteDAO solicitanteDAO = SolicitanteDAO.getInstance(); // cria uma instancia da classe SolicitanteDAO
		solicitanteDAO.cadastraSolicitante(solicitante); // persiste o solicitante no banco de dados
		
		return "/solicitante/adicionado"; // redireciona para a pagina adicionado.jsp
	}
}
