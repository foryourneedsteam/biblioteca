package br.com.biblioteca.web.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.biblioteca.dao.FuncionarioDAO;
import br.com.biblioteca.model.Funcionario;
import br.com.biblioteca.model.Usuario;

@Controller
@RequestMapping("/funcionario") // mapeia todas as requisicoes que comecem com /funcionario
public class FuncionarioController {
	
	private FuncionarioDAO funcionarioDAO;
	
	/*
	 * Recebe a requisicao com mapeamento /funcionario/novo
	 * Redireciona para a pagina novo.jsp na pasta funcionario
	 * So ira utilizar o mapeamento se a requisicao for pelo metodo GET
	 */
	@RequestMapping(value = "/novo", method = RequestMethod.GET)
	public String novo() {
		return "/funcionario/novo";
	}
	

	/*
	 * Recebe a requisicao com mapeamento /funcionario/salvar
	 * Inicia o tratamento para salvar um funcionario no banco de dados
	 * So ira utilizar o mapeamento se a requisicao for pelo metodo POST
	 * @Valid valida o objeto funcionario e usuario
	 * BindingResult permite verificar se algum erro de validacao foi encontrado no formulario
	 * IMPORTANTE: sempre manter um objeto do tipo BindingResult apos um objeto validado.
	 * Para esse caso foram 2 @Valid e 2 BindingResult (1 para cada classe a ser validada)
	 */
	@RequestMapping(value = "/salvar", method = RequestMethod.POST)
	public String salvar(@Valid Funcionario funcionario, BindingResult resultFuncionario,
			@Valid Usuario usuario, BindingResult resultUsuario) {
		// se algum erro for encontrado redireciona para a pagina do formulario
		if (resultFuncionario.hasErrors()) {
			return novo();
		}
		
		// se algum erro for encontrado redireciona para a pagina do formulario
		if (resultUsuario.hasErrors()) {
			return novo();
		}
		
		funcionario.setUsuario(usuario); // atribui o usuario ao funcionario
		funcionarioDAO = FuncionarioDAO.getInstance(); // cria uma instancia da classe FuncionarioDAO
		funcionarioDAO.cadastrarFuncionario(funcionario); // persiste o funcionario no banco de dados
		
		return "/funcionario/adicionado"; // redireciona para a pagina adicionado.jsp
	}
}
