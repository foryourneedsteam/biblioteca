package br.com.biblioteca.web.controller;

import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.biblioteca.dao.EmprestimoDAO;
import br.com.biblioteca.dao.ItemDAO;
import br.com.biblioteca.dao.SolicitanteDAO;
import br.com.biblioteca.model.Emprestimo;
import br.com.biblioteca.model.Item;
import br.com.biblioteca.model.Solicitante;
import br.com.biblioteca.model.Usuario;

@Controller
@RequestMapping("/emprestimo") // mapeia todas as requisicoes que comecem com /emprestimo
public class EmprestimoController {
	
	private SolicitanteDAO solicitanteDAO;
	private ItemDAO itemDAO;
	private EmprestimoDAO emprestimoDAO;
	
	/*
	 * Recebe a requisicao com mapeamento /emprestimo/novo
	 * Redireciona para a pagina novo.jsp na pasta item
	 * Utilizando a classe ModelAndView atribui ao objeto items a colecao de itens do banco de dados
	 * So ira utilizar o mapeamento se a requisicao for pelo metodo GET
	 */
	@RequestMapping(value = "/novo", method = RequestMethod.GET)
	public ModelAndView novo() {
		itemDAO = ItemDAO.getInstance();
		List<Item> items = itemDAO.getLista();
		
		ModelAndView modelAndView = new ModelAndView("/emprestimo/novo");
		modelAndView.addObject("items", items);
		
		return modelAndView;
	}
	
	/*
	 * Recebe a requisicao com mapeamento /emprestimo/salvar
	 * Redireciona para a pagina salvar.jsp na pasta emprestimo
	 * So ira utilizar o mapeamento se a requisicao for pelo metodo POST
	 */
	@RequestMapping(value = "/salvar", method = RequestMethod.POST)
	public ModelAndView salvar(String ra, Integer iditem, Emprestimo emprestimo) {
		ModelAndView modelAndView = new ModelAndView("/emprestimo/resumo");
		
		solicitanteDAO = SolicitanteDAO.getInstance();
		itemDAO = ItemDAO.getInstance();
		
		// usa a classe SolicitanteDAO para chamar o metodo e procurar pelo solicitante
		Solicitante solicitante = solicitanteDAO.getSolicitantePorRa(ra);
		
		/* 
		 * Se o solicitante nao for encontrado redireciona para a pagina resumo
		 * e atribui ao objeto erro a mensagem de erro 
		 */
		if (solicitante == null) {
			modelAndView.addObject("erro", "Solicitante não encontrado");
			return modelAndView;
		}
		
		// usa a classe ItemDAO para chamar o metodo e procurar pelo item
		Item item = itemDAO.pesquisaItemPorId(iditem);
		
		/*
		 * Se o item for uma edicao especial e o solicitante nao for um professor
		 * redireciona para a pagina resumo, atribui ao objeto erro a mensagem de erro
		 */
		if (item.isEdEspecial() && !solicitante.getUsuario().getTipo().equals(Usuario.TIPO_PROFESSOR)) {
			modelAndView.addObject("erro", "Edições especiais só pode ser solicitadas por professores");
			return modelAndView;
		}
		
		/*
		 * verifica se o número de emprestimos abertos desse solicitante é maior ou igual a 3
		 * Se o numero for maior ou igual a 3, redireciona para a pagina resumo.jsp e informa o erro
		 */
		emprestimoDAO = EmprestimoDAO.getInstance();
		int emprestimos = emprestimoDAO.getNumEmprestimosAbertosDoSolicitante(solicitante);
		
		if (emprestimos >= 3) {
			modelAndView.addObject("erro", "Não são permitidos mais de 3 empréstimos");
			return modelAndView;
		}
		
		/*
		 * Verifica se o item está emprestado
		 * Se o item estiver emprestado, redireciona para a pagina resumo.jsp
		 * E atribui ao objeto erro a mensagem de erro
		 */
		if (emprestimoDAO.isItemEmprestado(item)) {
			modelAndView.addObject("erro", "O item " + item.getDescricao() + " está emprestado");
			return modelAndView;
		}
		
		// inicia o processo para salvar o emprestimo
		emprestimo.setSolicitante(solicitante);
		emprestimo.setItem(item);
		emprestimo.setDataInicial(Calendar.getInstance());
		emprestimo.setEmprestado(true);
		
		emprestimoDAO.cadastraEmprestimo(emprestimo);
		
		modelAndView.addObject("mensagem", "Empréstimo salvo com sucesso!");
		
		return modelAndView;
	}
	
	/*
	 * Metodo utilizado para listar todos os emprestimos abertos
	 * Recebe a requisicao com mapeamento /emprestimo/lista
	 * Redireciona para a pagina lista.jsp na pasta emprestimo
	 * Utilizando a classe ModelAndView atribui ao objeto emprestimos a colecao de emprestimos do banco de dados
	 * So ira utilizar o mapeamento se a requisicao for pelo metodo GET
	 */
	@RequestMapping(value = "/lista", method = RequestMethod.GET)
	public ModelAndView lista() {
		emprestimoDAO = EmprestimoDAO.getInstance();
		
		ModelAndView modelAndView = new ModelAndView("/emprestimo/lista");
		modelAndView.addObject("emprestimos", emprestimoDAO.getEmprestimosAbertos());
		
		return modelAndView;
	}
	
	/*
	 * Metodo utilizado para listar todos os emprestimos de um solicitante
	 * Recebe a requisicao com mapeamento /emprestimo/meus-emprestimos
	 * Redireciona para a pagina lista.jsp na pasta emprestimo
	 * Utilizando a classe ModelAndView atribui ao objeto emprestimos a colecao de emprestimos do banco de dados
	 * So ira utilizar o mapeamento se a requisicao for pelo metodo GET
	 */
	@RequestMapping(value = "/meus-emprestimos", method = RequestMethod.GET)
	public ModelAndView meusEmprestimos(HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuarioLogado");
		
		solicitanteDAO = SolicitanteDAO.getInstance();
		Solicitante solicitante = solicitanteDAO.getSolicitantePeloUsuario(usuario);
		
		emprestimoDAO = EmprestimoDAO.getInstance();
		
		ModelAndView modelAndView = new ModelAndView("/emprestimo/lista");
		modelAndView.addObject("emprestimos", emprestimoDAO.getEmprestimosAbertosDoSolicitante(solicitante));
		
		return modelAndView;
	}
	
	/*
	 * Metodo utilizado salvar um emprestimo devolvido
	 * Recebe a requisicao com mapeamento /emprestimo/devolver
	 * Redireciona para o mapeamento /lista em emprestimo
	 * So ira utilizar o mapeamento se a requisicao for pelo metodo GET
	 */
	@RequestMapping(value = "/devolver", method = RequestMethod.GET)
	public String devolver(Integer id) {
		emprestimoDAO = EmprestimoDAO.getInstance();
		
		// consulta o emprestimo utilizando o parametro id
		Emprestimo emprestimo = emprestimoDAO.getEmprestimoPorId(id);
		
		// inicia o processo para salvar o emprestimo devolvido
		emprestimo.setEmprestado(false);
		emprestimo.setDataDevolucao(Calendar.getInstance());
		emprestimoDAO.salvarEmprestimoDevolvido(emprestimo);
		
		return "redirect:/emprestimo/lista";
	}
}
