package br.com.biblioteca.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Usuario {
	
	public static final String TIPO_ALUNO = "A";
	public static final String TIPO_FUNCIONARIO = "F";
	public static final String TIPO_PROFESSOR = "P";

	@Id // primary key
	@GeneratedValue(strategy = GenerationType.IDENTITY) // gerar ids automaticamente
	private Integer id;
	
	@NotEmpty // nao permitir campo nulo na aplicacao
	@Column(nullable = false, unique = true) // nao permitir campo nulo na tabela e valor unico para o campo
	private String login;

	@NotEmpty // nao permitir campo nulo na aplicacao
	@Column(nullable = false) // nao permitir campo nulo na tabela
	private String senha;
	
	@NotEmpty // nao permitir campo nulo na aplicacao
	@Column(nullable = false) // nao permitir campo nulo na tabela
	private String tipo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getTipoToString() {
		if (tipo.equals(TIPO_ALUNO)) {
			return "Aluno";
		} else if (tipo.equals(TIPO_FUNCIONARIO)) {
			return "Funcionario";
		} else if (tipo.equals(TIPO_PROFESSOR)) {
			return "Professor";
		} else {
			return "Inválido";
		}
	}
}
