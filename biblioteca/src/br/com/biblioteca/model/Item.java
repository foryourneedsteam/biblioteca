package br.com.biblioteca.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Item {
	
	@Id // primary key
	@GeneratedValue(strategy = GenerationType.IDENTITY) // gerar ids automaticamente
	private Integer id;
	
	private Integer tipo;
	
	@NotEmpty // nao permitir campo nulo na aplicacao
	@Column(nullable = false) // nao permitir campo nulo na tabela
	private String descricao;
	
	@NotEmpty // nao permitir campo nulo na aplicacao
	@Column(name = "tipoaquisicao", nullable = false) // nao permitir campo nulo na tabela
	private String tipoAquisicao;
	
	@Column(name = "valorcompra") 
	private double valorCompra;
	
	@Column(name = "edespecial") 
	private boolean edEspecial;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descicao) {
		this.descricao = descicao;
	}

	public String getTipoAquisicao() {
		return tipoAquisicao;
	}

	public void setTipoAquisicao(String tipoAquisicao) {
		this.tipoAquisicao = tipoAquisicao;
	}

	public double getValorCompra() {
		return valorCompra;
	}

	public void setValorCompra(double valorCompra) {
		this.valorCompra = valorCompra;
	}

	public boolean isEdEspecial() {
		return edEspecial;
	}

	public void setEdEspecial(boolean edEspecial) {
		this.edEspecial = edEspecial;
	}
	
	/*
	 * Metodo para retornar o tipo de aquisicao de forma completa
	 * Se o tipo for D retorna Doacao
	 * Se o tipo for C retorna C
	 * Se não for nem C ou D retorna invalido
	 */
	public String getTipoAquisicaoToString() {
		if (tipoAquisicao.equals("C")) {
			return "Compra";
		} else if (tipoAquisicao.equals("D")) {
			return "Doação";
		} else {
			return "Tipo inválido";
		}
	}
	
	/*
	 * Metodo para retornar o tipo do item de forma completa
	 * Se o tipo for 0 retorna Livro
	 * Se o tipo for 1 retorna Revista
	 */
	public String getTipoToString() {
		if (tipo == 0) {
			return "Livro";
		} else {
			return "Revista";
		}
	}
}
