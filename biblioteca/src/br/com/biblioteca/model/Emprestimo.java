package br.com.biblioteca.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import com.sun.istack.internal.NotNull;

@Entity
public class Emprestimo {

	@Id
	// primary key
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	// gerar ids automaticamente
	private Integer id;

	private boolean emprestado;

	// relacionamento muitos emprestimos para um solicitante, obrigatorio
	@ManyToOne(optional = false)
	private Solicitante solicitante;

	// relacionamento muitos emprestimos para um item, obrigatorio
	@ManyToOne(optional = false)
	private Item item;

	@NotNull
	// nao permitir campo nulo na aplicacao
	@Column(name = "datainicial", nullable = false)
	// nao permitir campo nulo na tabela
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	// informa ao Spring para converter o campo para data
	private Calendar dataInicial;

	@NotNull
	// nao permitir campo nulo na aplicacao
	@Column(name = "datafinal", nullable = false)
	// nao permitir campo nulo na tabela
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	// informa ao Spring para converter o campo para data
	private Calendar dataFinal;

	
	@Column(name = "datadevolucao")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	// informa ao Spring para converter o campo para data
	private Calendar dataDevolucao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isEmprestado() {
		return emprestado;
	}

	public void setEmprestado(boolean emprestado) {
		this.emprestado = emprestado;
	}

	public Solicitante getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(Solicitante solicitante) {
		this.solicitante = solicitante;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Calendar getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Calendar dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Calendar getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Calendar dataFinal) {
		this.dataFinal = dataFinal;
	}

	public Calendar getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(Calendar dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}
}
