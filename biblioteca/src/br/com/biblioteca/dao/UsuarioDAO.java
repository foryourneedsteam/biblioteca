package br.com.biblioteca.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import br.com.biblioteca.model.Usuario;
import br.com.biblioteca.persistence.util.PersistenceUtil;

public class UsuarioDAO {

	private EntityManager entityManager;
	private static UsuarioDAO instance;
	
	/*
	 * Esse metodo sera utilizado para criar uma unica instancia da classe UsuarioDAO
	 * Ele verifica se o objeto instance, da classe UsuarioDAO, nao foi criado (instance == null)
	 * Se o objeto instance nao foi criadao, entao uma instancia da classe é criada atraves do construtor padrao UsuarioDAO()
	 * O objeto instance é retornada para ser utilizado
	 */
	public static UsuarioDAO getInstance() {
		if (instance == null) {
			instance = new UsuarioDAO();
		}
		return instance;
	}
	
	/*
	 * O construtor foi declarado como privado para que um objeto da classe UsuarioDAO
	 * so possa ser criado obrigatoriamente utilizando o metodo getInstance()
	 * O construtor atribui ao objeto entityManager o metodo getEntityManager
	 */
	private UsuarioDAO() {
		// TODO Auto-generated constructor stub
		entityManager = getEntityManager();
	}
	
	/* 
	 * O metodo getEntityManager() retorna um objeto da Classe EntityManager
	 * Ele atribui ao objeto factory, da classe EntityManagerFactory, a instancia dessa mesma classe
	 * utilizando o metodo getInstance da classe PersistenceUtil
	 * Se o objeto entityManager, da classe UsuarioDAO, for nulo, uma instancia é criada
	 * o objeto entityManager é retornado
	 */
	private EntityManager getEntityManager() {
		EntityManagerFactory factory = PersistenceUtil.getInstance();
		if (entityManager == null) {
			entityManager = factory.createEntityManager();
		}
		return entityManager;
	}
	
	/*
	 * Metodo utilizado para cadastar um usuario
	 * Utilizada o objeto usuario da classe Usuario para salvar um usuario no banco de dados
	 */
	public void cadastraUsuario(Usuario usuario) {
		getEntityManager().getTransaction().begin();
		// cadastro do objeto usuario no banco de dados
		getEntityManager().persist(usuario);
		// commit da transacao
		getEntityManager().getTransaction().commit();
	}
	
	/*
	 * Metodo utilizado para autenticar um usuario
	 * Utilizado o objeto usuario da classe Usuario para consultar um usuario no banco de dados
	 */
	public Usuario autenticarUsuario(Usuario usuario) {
		getEntityManager().getTransaction().begin();
		// monta a query para consultar um usuario no banco de dados
		Query query = getEntityManager().createQuery("from Usuario u where u.login = :login and u.senha = :senha");
		query.setParameter("login", usuario.getLogin());
		query.setParameter("senha", usuario.getSenha());
		// commit da transacao
		getEntityManager().getTransaction().commit();
		
		try {
			return (Usuario) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	/*void pesquisaUsuario(Usuario usuario) {
		try {
			manager = factory.createEntityManager();
			Usuario pesquisar = manager.find(Usuario.class, usuario.getId());

		} catch (Exception e) {

		} finally {
			manager.close();
		}

	}

	void removeUsuario(Usuario usuario) {
		try {
			Usuario encontrado = manager.find(Usuario.class, usuario.getId());
			manager.getTransaction().begin();
			manager.remove(encontrado);
		} catch (Exception e) {

		} finally {
			manager.close();
		}

	}

	void atualizaUsuaro(Usuario usuario) {

		try {
			manager.getTransaction().begin();
			manager.merge(usuario);
			manager.getTransaction().commit();
		} catch (Exception e) {

		} finally {
			manager.close();
		}
	}*/
}
