package br.com.biblioteca.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import br.com.biblioteca.model.Funcionario;
import br.com.biblioteca.persistence.util.PersistenceUtil;

public class FuncionarioDAO {

	private EntityManager entityManager;
	private static FuncionarioDAO instance;
	
	/*
	 * Esse metodo sera utilizado para criar uma unica instancia da classe FuncionarioDAO
	 * Ele verifica se o objeto instance, da classe FuncionarioDAO, nao foi criado (instance == null)
	 * Se o objeto instance nao foi criadao, entao uma instancia da classe é criada atraves do construtor padrao FuncionarioDAO()
	 * O objeto instance é retornada para ser utilizado
	 */
	public static FuncionarioDAO getInstance() {
		if (instance == null) {
			instance = new FuncionarioDAO();
		}
		return instance;
	}
	
	/*
	 * O construtor foi declarado como privado para que um objeto da classe FuncionarioDAO
	 * so possa ser criado obrigatoriamente utilizando o metodo getInstance()
	 * O construtor atribui ao objeto entityManager o metodo getEntityManager
	 */
	private FuncionarioDAO() {
		// TODO Auto-generated constructor stub
		entityManager = getEntityManager();
	}
	
	/* 
	 * O metodo getEntityManager() retorna um objeto da Classe EntityManager
	 * Ele atribui ao objeto factory, da classe EntityManagerFactory, a instancia dessa mesma classe
	 * utilizando o metodo getInstance da classe PersistenceUtil
	 * Se o objeto entityManager, da classe FuncionarioDAO, for nulo, uma instancia é criada
	 * o objeto entityManager é retornado
	 */
	private EntityManager getEntityManager() {
		EntityManagerFactory factory = PersistenceUtil.getInstance();
		if (entityManager == null) {
			entityManager = factory.createEntityManager();
		}
		return entityManager;
	}

	/*
	 * Metodo utilizado para persistir no banco de dados um objeto da classe Funcionario
	 */
	public void cadastrarFuncionario(Funcionario funcionario) {
		// inicio da transacao
		getEntityManager().getTransaction().begin();
		// cadastro do objeto funcionario no banco de dados
		getEntityManager().persist(funcionario);
		// commit da transacao
		getEntityManager().getTransaction().commit();
	}	
	
	/*
	void pesquisaFuncionario(Funcionario funcionario) {

		try {
			manager = factory.createEntityManager();
			Funcionario pesquisar = manager.find(Funcionario.class,
					funcionario.getId());

		} catch (Exception e) {

		} finally {
			manager.close();
		}

	}

	void removeFuncionario(Funcionario funcionario) {

		try {
			Funcionario encontrado = manager.find(Funcionario.class,
					funcionario.getId());
			manager.getTransaction().begin();
			manager.remove(encontrado);

		} catch (Exception e) {

		} finally {
			manager.close();
		}
	}

	void atualizaFuncionario(Funcionario funcionario) {

		try {
			manager.getTransaction().begin();
			manager.merge(funcionario);
			manager.getTransaction().commit();
		} catch (Exception e) {

		} finally {
			manager.close();
		}
	}*/
}
