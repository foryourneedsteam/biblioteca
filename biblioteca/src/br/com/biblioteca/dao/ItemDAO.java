package br.com.biblioteca.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import br.com.biblioteca.model.Item;
import br.com.biblioteca.persistence.util.PersistenceUtil;

/*
 * A classe ItemDAO sera utilizada no projeto com apenas uma instancia
 * Desse forma nao sera preciso criar diversos objetos dessa classe
 * Para isso o objeto itemDAO foi declarado como static
 * O ojeto itemDAO ira acessar o banco de dados com uma unica instancia
 */
public class ItemDAO {

	private EntityManager entityManager;
	private static ItemDAO instance;
	
	/*
	 * Esse metodo sera utilizado para criar uma unica instancia da classe ItemDAO
	 * Ele verifica se o objeto instance, da classe ItemDAO, nao foi criado (instance == null)
	 * Se o objeto instance nao foi criadao, entao uma instancia da classe é criada atraves do construtor padrao ItemDAO()
	 * O objeto instance é retornada para ser utilizado
	 */
	public static ItemDAO getInstance() {
		if (instance == null) {
			instance = new ItemDAO();
		}
		return instance;
	}
	
	/*
	 * O construtor foi declarado como privado para que um objeto da classe ItemDAO
	 * so possa ser criado obrigatoriamente utilizando o metodo getInstance()
	 * O construtor atribui ao objeto entityManager o metodo getEntityManager
	 */
	private ItemDAO() {
		// TODO Auto-generated constructor stub
		entityManager = getEntityManager();
	}
	
	/* 
	 * O metodo getEntityManager() retorna um objeto da Classe EntityManager
	 * Ele atribui ao objeto factory, da classe EntityManagerFactory, a instancia dessa mesma classe
	 * utilizando o metodo getInstance da classe PersistenceUtil
	 * Se o objeto entityManager, da classe ItemDAO, for nulo, uma instancia é criada
	 * o objeto entityManager é retornado
	 */
	private EntityManager getEntityManager() {
		EntityManagerFactory factory = PersistenceUtil.getInstance();
		if (entityManager == null) {
			entityManager = factory.createEntityManager();
		}
		return entityManager;
	}
	
	/*
	 * Metodo utilizado para persistir no banco de dados um objeto da classe Item
	 */
	public void cadastraItem(Item item) {
		// inicio da transacao
		getEntityManager().getTransaction().begin();
		// cadastro do objeto item no banco de dados
		getEntityManager().persist(item);
		// commit da transacao
		getEntityManager().getTransaction().commit();
	}

	/*
	 * Metodo utilizado para retorna uma colecao de objetos da classe Item 
	 */
	@SuppressWarnings("unchecked")
	public List<Item> getLista() {
		// inicio da transacao
		getEntityManager().getTransaction().begin();
		// construcao da Query para consulta dos itens			
		Query query = getEntityManager().createQuery("from Item i order by i.descricao");			
		// commit da transacao
		getEntityManager().getTransaction().commit();
		
		return query.getResultList();
	}
	
	/*
	 * Metodo utilizado para retorna um objeto Item atraves da propriedade id 
	 */
	public Item pesquisaItemPorId(Integer id) {
		// inicio da transacao
		getEntityManager().getTransaction().begin();
		// construcao da Query para consulta dos itens			
		Query query = getEntityManager().createQuery("from Item i where i.id = :id");
		query.setParameter("id", id);
		// commit da transacao
		getEntityManager().getTransaction().commit();
		
		try {
			return (Item) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
}
