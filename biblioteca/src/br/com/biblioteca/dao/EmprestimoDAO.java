package br.com.biblioteca.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import br.com.biblioteca.model.Emprestimo;
import br.com.biblioteca.model.Item;
import br.com.biblioteca.model.Solicitante;
import br.com.biblioteca.persistence.util.PersistenceUtil;

public class EmprestimoDAO {

	private EntityManager entityManager;
	private static EmprestimoDAO instance;
	
	/*
	 * Esse metodo sera utilizado para criar uma unica instancia da classe EmprestimoDAO
	 * Ele verifica se o objeto instance, da classe EmprestimoDAO, nao foi criado (instance == null)
	 * Se o objeto instance nao foi criadao, entao uma instancia da classe é criada atraves do construtor padrao EmprestimoDAO()
	 * O objeto instance é retornada para ser utilizado
	 */
	public static EmprestimoDAO getInstance() {
		if (instance == null) {
			instance = new EmprestimoDAO();
		}
		return instance;
	}
	
	/*
	 * O construtor foi declarado como privado para que um objeto da classe EmprestimoDAO
	 * so possa ser criado obrigatoriamente utilizando o metodo getInstance()
	 * O construtor atribui ao objeto entityManager o metodo getEntityManager
	 */
	private EmprestimoDAO() {
		// TODO Auto-generated constructor stub
		entityManager = getEntityManager();
	}
	
	/* 
	 * O metodo getEntityManager() retorna um objeto da Classe EntityManager
	 * Ele atribui ao objeto factory, da classe EntityManagerFactory, a instancia dessa mesma classe
	 * utilizando o metodo getInstance da classe PersistenceUtil
	 * Se o objeto entityManager, da classe EmprestimoDAO, for nulo, uma instancia é criada
	 * o objeto entityManager é retornado
	 */
	private EntityManager getEntityManager() {
		EntityManagerFactory factory = PersistenceUtil.getInstance();
		if (entityManager == null) {
			entityManager = factory.createEntityManager();
		}
		return entityManager;
	}
	
	/*
	 * Metodo utilizado para persistir no banco de dados um objeto da classe Empestimo
	 */
	public void cadastraEmprestimo(Emprestimo emprestimo) {
		// inicio da transacao
		getEntityManager().getTransaction().begin();
		// cadastro do objeto item no banco de dados
		getEntityManager().persist(emprestimo);
		// commit da transacao
		getEntityManager().getTransaction().commit();
	}
	
	/*
	 * Metodo utilizado para pesquisar o numero de emprestimos abertos de um solicitante
	 * Utiliza um objeto solicitante para fazer a consulta
	 */
	public int getNumEmprestimosAbertosDoSolicitante(Solicitante solicitante) {
		// inicio da transacao
		getEntityManager().getTransaction().begin();
		// monta a query para fazer a consulta no banco de dados
		Query query = getEntityManager().createQuery("from Emprestimo e where e.emprestado = true and e.solicitante = :solicitante");
		query.setParameter("solicitante", solicitante);
		// commit da transacao
		getEntityManager().getTransaction().commit();
		
		return query.getResultList().size();
	}
	
	/*
	 * Metodo utilizado para pesquisar os emprestimos de um solicitante
	 * Utiliza um objeto solicitante para fazer a consulta
	 */
	@SuppressWarnings("unchecked")
	public List<Emprestimo> getEmprestimosAbertosDoSolicitante(Solicitante solicitante) {
		// inicio da transacao
		getEntityManager().getTransaction().begin();
		// monta a query para fazer a consulta no banco de dados
		Query query = getEntityManager().createQuery("from Emprestimo e where e.solicitante = :solicitante");
		query.setParameter("solicitante", solicitante);
		// commit da transacao
		getEntityManager().getTransaction().commit();
		
		return query.getResultList();
	}
	
	/* Metodo utilizado para verificar se um item está emprestado
	 * Utiliza o objeto item da classe Item como parametro
	 */
	public boolean isItemEmprestado(Item item) {
		// inicio da transacao
		getEntityManager().getTransaction().begin();
		// monta a query para fazer a consulta no banco de dados
		Query query = getEntityManager().createQuery("from Emprestimo e where e.emprestado = true and e.item = :item");
		query.setParameter("item", item);
		// commit da transacao
		getEntityManager().getTransaction().commit();
		
		if (query.getResultList().size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/*
	 * Mtodo utilizado para consultar no banco de dados todos os emprestimos abertos
	 */
	@SuppressWarnings("unchecked")
	public List<Emprestimo> getEmprestimosAbertos() {
		// inicio da transacao
		getEntityManager().getTransaction().begin();
		// monta a query para fazer a consulta no banco de dados
		Query query = getEntityManager().createQuery("from Emprestimo e where e.emprestado = true");
		// commit da transacao
		getEntityManager().getTransaction().commit();
		
		return query.getResultList();
	}
	
	/*
	 * Metodo utilizado retornar um emprestimo atraves do seu id
	 * Utiliza o parametro id para consultar o emprestimo no banco de dados
	 */
	public Emprestimo getEmprestimoPorId(Integer id) {		
		return (Emprestimo) getEntityManager().find(Emprestimo.class, id);
	}
	
	/*
	 * Metodo utilizado para salvar um emprestimo devolvido
	 * Utiliza o objeto emprestimo da classe Emprestimo para salvar as informacoes no banco de dados
	 */
	public void salvarEmprestimoDevolvido(Emprestimo emprestimo) {
		// inicio da transacao
		getEntityManager().getTransaction().begin();
		// monta a query para fazer a consulta no banco de dados
		getEntityManager().merge(emprestimo);
		// commit da transacao
		getEntityManager().getTransaction().commit();
	}
}
