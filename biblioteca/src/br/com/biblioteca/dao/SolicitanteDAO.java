package br.com.biblioteca.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import br.com.biblioteca.model.Solicitante;
import br.com.biblioteca.model.Usuario;
import br.com.biblioteca.persistence.util.PersistenceUtil;

public class SolicitanteDAO {
	
	private EntityManager entityManager;
	private static SolicitanteDAO instance;
	
	/*
	 * Esse metodo sera utilizado para criar uma unica instancia da classe SolicitanteDAO
	 * Ele verifica se o objeto instance, da classe SolicitanteDAO, nao foi criado (instance == null)
	 * Se o objeto instance nao foi criadao, entao uma instancia da classe é criada atraves do construtor padrao SolicitanteDAO()
	 * O objeto instance é retornada para ser utilizado
	 */
	public static SolicitanteDAO getInstance() {
		if (instance == null) {
			instance = new SolicitanteDAO();
		}
		return instance;
	}
	
	/*
	 * O construtor foi declarado como privado para que um objeto da classe SolicitanteDAO
	 * so possa ser criado obrigatoriamente utilizando o metodo getInstance()
	 * O construtor atribui ao objeto entityManager o metodo getEntityManager
	 */
	private SolicitanteDAO() {
		// TODO Auto-generated constructor stub
		entityManager = getEntityManager();
	}
	
	/* 
	 * O metodo getEntityManager() retorna um objeto da Classe EntityManager
	 * Ele atribui ao objeto factory, da classe EntityManagerFactory, a instancia dessa mesma classe
	 * utilizando o metodo getInstance da classe PersistenceUtil
	 * Se o objeto entityManager, da classe SolicitanteDAO, for nulo, uma instancia é criada
	 * o objeto entityManager é retornado
	 */
	private EntityManager getEntityManager() {
		EntityManagerFactory factory = PersistenceUtil.getInstance();
		if (entityManager == null) {
			entityManager = factory.createEntityManager();
		}
		return entityManager;
	}

	/*
	 * Metodo utilizado para persistir no banco de dados um objeto da classe Solicitante
	 */
	public void cadastraSolicitante(Solicitante solicitante) {
		// inicio da transacao
		getEntityManager().getTransaction().begin();
		// cadastro do objeto solicitante no banco de dados
		getEntityManager().persist(solicitante);
		// commit da transacao
		getEntityManager().getTransaction().commit();
	}
	
	/*
	 * Metodo utilizado para consultar no banco de dados um objeto da classe Solicitante atraves da propriedade RA
	 */
	public Solicitante getSolicitantePorRa(String ra) {
		// inicio da transacao
		getEntityManager().getTransaction().begin();
		// construcao da Query para consulta dos itens			
		Query query = getEntityManager().createQuery("from Solicitante s where s.ra = :ra");
		query.setParameter("ra", ra);
		// commit da transacao
		getEntityManager().getTransaction().commit();
		 
		try {
			return (Solicitante) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	/*
	 * Metodo utilizado para retornar o soliciante do usuario
	 * Utiliza o parametro usuario para retornar o solicitante
	 */
	public Solicitante getSolicitantePeloUsuario(Usuario usuario) {
		// inicio da transacao
		getEntityManager().getTransaction().begin();
		// monta a query para consultar o funcionario no banco de dados
		Query query = getEntityManager().createQuery("from Solicitante s where s.usuario = :usuario");
		query.setParameter("usuario", usuario);
		// commit da transacao
		getEntityManager().getTransaction().commit();
		
		return (Solicitante) query.getSingleResult();
	}
}
