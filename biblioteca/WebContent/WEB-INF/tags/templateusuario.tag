<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ attribute name="title" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Biblioteca | ${title }</title>
</head>
<body>	
	<c:if test="${usuarioLogado ne null }">
		<h3>
			<a href="/biblioteca/">Home</a><br />
		</h3>
		Bem-vindo ${sessionScope.usuarioLogado.login }
		<a href='<c:url value="/logout" />'>Logout</a>
	</c:if>
	
	<h1>
		${title }
	</h1>
	
	<jsp:doBody />
	
	<hr />
	<h1>
		${footer }
	</h1>
</body>
</html>