<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tags:templateusuario title="Novo funcionário">
	<jsp:body>
		<h2>Funcionário</h2>
		<form action="salvar" method="post">
			<input type="hidden" name="tipo" value="F" />
			<table>
				<tr>
					<th>Nome</th>
					<td>
						<input type="text" name="nome" size="20" required="required" />
						<form:errors path="funcionario.nome" />
					</td>
				</tr>
			</table>
			
			<hr />
			
			<h2>Usuário</h2>
			
			<table>
				<tr>
					<th>Usuário</th>
					<td>
						<input type="text" name="login" required="required" />
						<form:errors path="usuario.login" />
					</td>
				</tr>
				<tr>
					<th>Senha</th>
					<td>
						<input type="password" name="senha" required="required" />
						<form:errors path="usuario.senha" />
					</td>
				</tr>
			</table>
			
			<input type="submit" value="Salvar" />
		</form>
	</jsp:body>
</tags:templateusuario>