<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tags:templateusuario title="Login">
	<form action="autenticar" method="post">
		<c:if test="${!empty param.erro }">
			<h3>Usuário ou senha invalidos</h3>
		</c:if>
		<table>
			<tr>
				<th>Login</th>
				<td>
					<input type="text" name="login" required="required" />
				</td>
			</tr>
			<tr>
				<th>Senha</th>
				<td>
					<input type="password" name="senha" required="required" />
				</td>
			</tr>
		</table>
		<input type="submit" value="Login" />
	</form>
</tags:templateusuario>