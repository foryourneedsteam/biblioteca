<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tags:templateusuario title="Novo solicitante">
	<jsp:body>
		<h2>Soliciante</h2>
		<form action="salvar" method="post">
			<table>
				<tr>
					<th>Nome</th>
					<td>
						<input type="text" name="nome" size="20" />
						<form:errors path="solicitante.nome" />
					</td>
				</tr>
				<tr>
					<th>RA</th>
					<td>
						<input type="text" name="ra" size="20" />
						<form:errors path="solicitante.ra" />
					</td>
				</tr>
				<tr>
					<th>Tipo</th>
					<td>
						<select name="tipo">
							<option value="A">Aluno</option>
							<option value="P">Professor</option>
						</select>
						<form:errors path="usuario.tipo" />
					</td>
				</tr>
			</table>
			
			<hr />
			
			<h2>Usuário</h2>
			
			<table>
				<tr>
					<th>Usuário</th>
					<td>
						<input type="text" name="login" />
						<form:errors path="usuario.login" />
					</td>
				</tr>
				<tr>
					<th>Senha</th>
					<td>
						<input type="password" name="senha" />
						<form:errors path="usuario.senha" />
					</td>
				</tr>
			</table>
			
			<input type="submit" value="Salvar" />
		</form>
	</jsp:body>
</tags:templateusuario>