<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tags:templateusuario title="Novo empréstimo">
	<jsp:body>
		<form:errors />
		
		<form action="salvar" method="post">
			<table>
				<tr>
					<th>RA solicitante</th>
					<td>
						<input type="text" name="ra" required="required" />
					</td>
				</tr>
				<tr>
					<th>Item</th>
					<td>
						<select name="iditem">
							<c:forEach items="${items }" var="item">
								<option value="${item.id }">${item.descricao }</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>Data final</th>
					<td>
						<input type="text" name="dataFinal" required="required" />
					</td>
				</tr>
			</table>
			
			<input type="submit" value="Salvar" />
		</form>
	</jsp:body>
</tags:templateusuario>