<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tags:templateusuario title="Solicitação empréstimo">
	<c:if test="${!empty erro }">
		<h2 style="color: red;">${erro }</h2>
	</c:if>
	
	<c:if test="${!empty mensagem }">
		<h2>${mensagem }</h2>
	</c:if>
</tags:templateusuario>