<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<tags:templateusuario title="Empréstimos abertos">
	<table border="1">
		<tr>
			<th>Item</th>
			<th>Ed. Especial</th>
			<th>Data</th>
			<th>Data final</th>
			<th>Emprestado</th>
			<th>Data devolução</th>
			<th>Solicitante</th>
			<th>Ações</th>			
		</tr>
		<c:forEach items="${emprestimos }" var="emprestimo">
			<tr>
				<td>${emprestimo.item.descricao }</td>
				<td>${emprestimo.item.edEspecial ? 'Sim' : 'Não' }</td>
				<td><fmt:formatDate value="${emprestimo.dataInicial.time }"/></td>
				<td><fmt:formatDate value="${emprestimo.dataFinal.time }"/></td>
				<td>${emprestimo.emprestado ? 'Sim' : 'Não' }</td>
				<td><fmt:formatDate value="${emprestimo.dataDevolucao.time }"/></td>
				<td>${emprestimo.solicitante.nome } (${emprestimo.solicitante.usuario.tipoToString })</td>
				<c:if test="${usuarioLogado.tipo eq 'F' }">
					<td><a href="devolver?id=${emprestimo.id }">Registar devolução</a></td>
				</c:if>
			</tr>
		</c:forEach>
	</table>
</tags:templateusuario>