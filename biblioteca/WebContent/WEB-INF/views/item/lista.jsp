<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tags:templateusuario title="Listagem de itens">
	<jsp:body>
		<table>
			<tr>
				<th>ID</th>
				<th>Descrição</th>
				<th>Tipo</th>
				<th>Aquisição</th>
				<th>Valor compra</th>
				<th>Edição especial</th>
			</tr>
			<c:forEach items="${items }" var="item">
				<tr>
					<td>${item.id }</td>
					<td>${item.descricao }</td>
					<td>${item.tipoToString }
					<td>${item.tipoAquisicaoToString }
					<td>${item.valorCompra }</td>
					<td>${item.edEspecial ? 'Sim' : 'Não' }</td>
				</tr>
			</c:forEach>
		</table>
	</jsp:body>
</tags:templateusuario>