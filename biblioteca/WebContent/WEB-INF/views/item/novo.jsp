<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tags:templateusuario title="Novo item">
	<jsp:body>
		<form action="salvar" method="post">			
			<table>
				<tr>
					<th>
						Descri��o						
					</th>
					<td>
						<input type="text" name="descricao" size="25" />
						<form:errors path="item.descricao" />
					</td>
				</tr>
				<tr>
					<th>
						Tipo						
					</th>
					<td>
						<input type="radio" name="tipo" value="0" />Livro
						<input type="radio" name="tipo" value="1" />Revista
						<form:errors path="item.tipo" />
					</td>
				</tr>
				<tr>
					<th>
						Tipo aquisi��o						
					</th>
					<td>
						<input type="radio" name="tipoAquisicao" value="C" />Compra
						<input type="radio" name="tipoAquisicao" value="D" />Doa��o
						<form:errors path="item.tipoAquisicao" />
					</td>
				</tr>
				<tr>
					<th>
						Valor compra						
					</th>
					<td>
						<input type="text" name="valorCompra" size="10" value="0.0" />
						<form:errors path="item.valorCompra" />
					</td>
				</tr>
				<tr>
					<th>
						Edi��o especial						
					</th>
					<td>
						<input type="checkbox" name="edEspecial" />
						<form:errors path="item.edEspecial" />
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="submit" value="Salvar" />
					</td>
				</tr>
			</table>
		</form>
	</jsp:body>	
</tags:templateusuario>