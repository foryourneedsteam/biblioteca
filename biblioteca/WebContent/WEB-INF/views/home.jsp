<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<template:templateusuario title="Biblioteca">
	<jsp:body>
		<c:if test="${usuarioLogado.tipo eq 'F' }">
			<h3>Itens</h3>
			
			<a href="<c:url value="/item/novo" />">Novo item</a>
			<a href="<c:url value="/item/lista" />">Listar itens</a>
		</c:if>
		
		<c:if test="${usuarioLogado.tipo eq 'M' }">
			<h3>Solicitante</h3>
			<a href="<c:url value="/solicitante/novo" />">Novo solicitante</a>
		</c:if>
		
		<h3>Empréstimo</h3>
		<c:if test="${usuarioLogado.tipo eq 'F' }">			
			<a href="<c:url value="/emprestimo/novo" />">Novo empréstimo</a><br />						
		</c:if>
		<c:if test="${usuarioLogado.tipo eq 'A' or usuarioLogado.tipo eq 'P' }">
			<a href="<c:url value="/emprestimo/meus-emprestimos" />">Meus empréstimos</a>			
		</c:if>
		<c:if test="${usuarioLogado.tipo eq 'F' }">
			<a href="<c:url value="/emprestimo/lista" />">Abertos</a>
		</c:if>
		<c:if test="${usuarioLogado.tipo eq 'M' }">
			<h3>Funcionário</h3>
			<a href='<c:url value="/funcionario/novo" />'>Novo funcionário</a>
		</c:if>
	</jsp:body>	
</template:templateusuario>