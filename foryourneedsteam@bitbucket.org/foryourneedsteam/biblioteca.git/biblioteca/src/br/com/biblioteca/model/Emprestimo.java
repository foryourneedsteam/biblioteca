package br.com.biblioteca.model;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sun.istack.internal.NotNull;

@Entity
public class Emprestimo {

	@Id
	@GeneratedValue
	private Integer id;
	private boolean emprestado;
	@NotNull
	private Solicitante solicitante;
	@Temporal(TemporalType.DATE)
	private Calendar dt_inicial;
	@Temporal(TemporalType.DATE)
	private Calendar dt_final;



	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public boolean isEmprestado() {
		return emprestado;
	}
	public void setEmprestado(boolean emprestado) {
		this.emprestado = emprestado;
	}
	public Solicitante getSolicitante() {
		return solicitante;
	}
	public void setSolicitante(Solicitante solicitante) {
		this.solicitante = solicitante;
	}
	public Calendar getDt_inicial() {
		return dt_inicial;
	}
	public void setDt_inicial(Calendar dt_inicial) {
		this.dt_inicial = dt_inicial;
	}
	public Calendar getDt_final() {
		return dt_final;
	}
	public void setDt_final(Calendar dt_final) {
		this.dt_final = dt_final;
	}




}
