package br.com.biblioteca.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Usuario {

	@Id // primary key
	@GeneratedValue(strategy = GenerationType.IDENTITY) // gerar ids automaticamente
	private Integer id;
	
	@NotNull // nao permitir campo nulo na aplicacao
	@Column(nullable = false, unique = true) // nao permitir campo nulo na tabela e valor unico para o campo
	private String login;

	@NotNull // nao permitir campo nulo na aplicacao
	@Column(nullable = false) // nao permitir campo nulo na tabela
	private String senha;	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
