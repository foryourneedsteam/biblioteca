package br.com.biblioteca.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Funcionario extends Usuario {

	@Id // primary key
	@GeneratedValue(strategy = GenerationType.IDENTITY) // gerar ids automaticamente
	private Integer id;

	@NotNull // nao permitir campo nulo na aplicacao
	@Column(nullable = false) // nao permitir campo nulo na tabela
	private String nome;
	
	// relacionamento um para um  obrigatorio
	@OneToOne(optional = false, cascade = CascadeType.PERSIST)
	private Usuario usuario;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
