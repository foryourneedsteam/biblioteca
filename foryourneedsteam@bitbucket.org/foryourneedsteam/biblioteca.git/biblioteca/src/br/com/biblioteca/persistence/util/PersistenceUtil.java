package br.com.biblioteca.persistence.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceUtil {
	
	private static EntityManagerFactory entityManagerFactory;
	
	/*
	 * Retorna uma única instância do EntityManagerFactory
	 * para todo projeto
	 */
	public static EntityManagerFactory getInstance(){
		if(entityManagerFactory == null){
			entityManagerFactory = Persistence.createEntityManagerFactory("biblioteca");
		}
		return entityManagerFactory;
	}

}
