package br.com.biblioteca.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Item {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private Integer tipo;
	
	@NotNull
	@Column(nullable = false)
	private String descicao;
	
	@NotNull
	@Column(name = "tipoaquisicao", nullable = false)
	private String tipoAquisicao;
	
	@Column(name = "valorcompra") 
	private double valorCompra;
	
	@Column(name = "edespecial") 
	private boolean edEspecial;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public String getDescicao() {
		return descicao;
	}

	public void setDescicao(String descicao) {
		this.descicao = descicao;
	}

	public String getTipoAquisicao() {
		return tipoAquisicao;
	}

	public void setTipoAquisicao(String tipoAquisicao) {
		this.tipoAquisicao = tipoAquisicao;
	}

	public double getValorCompra() {
		return valorCompra;
	}

	public void setValorCompra(double valorCompra) {
		this.valorCompra = valorCompra;
	}

	public boolean isEdEspecial() {
		return edEspecial;
	}

	public void setEdEspecial(boolean edEspecial) {
		this.edEspecial = edEspecial;
	}
}
