package br.com.biblioteca.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import br.com.biblioteca.dao.util.DAOException;
import br.com.biblioteca.model.Item;
import br.com.biblioteca.persistence.util.PersistenceUtil;

public class ItemDAO {

	private EntityManager entityManager;
	private static ItemDAO instance;

	public static ItemDAO getInstance() {
		if (instance == null) {
			instance = new ItemDAO();
		}
		return instance;
	}

	private ItemDAO() {
		// TODO Auto-generated constructor stub
		entityManager = getEntityManager();
	}

	private EntityManager getEntityManager() {
		EntityManagerFactory factory = PersistenceUtil.getInstance();
		if (entityManager == null) {
			entityManager = factory.createEntityManager();
		}
		return entityManager;
	}

	public void cadastraItem(Item item) throws DAOException {
		try {			
			getEntityManager().getTransaction().begin();
			getEntityManager().persist(item);
			getEntityManager().getTransaction().commit();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	/* void pesquisaItem(Item item) {

		try {

			manager = factory.createEntityManager();
			Item pesquisar = manager.find(Item.class, item.getId());

		} catch (Exception e) {

		} finally {
			manager.close();
		}
	}

	void removeItem(Item item) {
		try {
			Item encontrado = manager.find(Item.class, item.getId());
			manager.getTransaction().begin();
			manager.remove(encontrado);

		} catch (Exception e) {

		} finally {
			manager.close();
		}
	}

	void atualizaItem(Item item) {

		try {
			manager.getTransaction().begin();
			manager.merge(item);
			manager.getTransaction().commit();
		} catch (Exception e) {

		} finally {
			manager.close();
		}
	} */
}
