package br.com.biblioteca.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.biblioteca.model.Usuario;

public class UsuarioDAO {

	private EntityManagerFactory factory;
	private EntityManager manager;

	void cadastraUsuario(Usuario usuario) {
		try {
			factory = Persistence.createEntityManagerFactory("usuario");
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			manager.persist(usuario);
			manager.getTransaction().commit();
		} catch (Exception e) {

		} finally {
			manager.close();
		}
	}

	void pesquisaUsuario(Usuario usuario) {
		try {
			manager = factory.createEntityManager();
			Usuario pesquisar = manager.find(Usuario.class, usuario.getId());

		} catch (Exception e) {

		} finally {
			manager.close();
		}

	}

	void removeUsuario(Usuario usuario) {
		try {
			Usuario encontrado = manager.find(Usuario.class, usuario.getId());
			manager.getTransaction().begin();
			manager.remove(encontrado);
		} catch (Exception e) {

		} finally {
			manager.close();
		}

	}

	void atualizaUsuaro(Usuario usuario) {

		try {
			manager.getTransaction().begin();
			manager.merge(usuario);
			manager.getTransaction().commit();
		} catch (Exception e) {

		} finally {
			manager.close();
		}
	}
}
