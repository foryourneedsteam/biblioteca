package br.com.biblioteca.web.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.biblioteca.dao.ItemDAO;
import br.com.biblioteca.dao.util.DAOException;
import br.com.biblioteca.model.Item;

@Controller
@RequestMapping("/item")
public class ItemController {
	
	@RequestMapping("/")
	public String home() {
		System.out.println("Teste");
		return "teste";
	}
	
	@RequestMapping(value = "/novo", method = RequestMethod.GET)
	public ModelAndView novo(ModelAndView modelAndView) {
		return new ModelAndView("item/novo", "item", new Item());		
	}
	
	@RequestMapping(value = "/salvar", method = RequestMethod.POST)
	public ModelAndView salvar(@Valid @ModelAttribute("item") Item item, BindingResult result) {
		ModelAndView modelAndView;
		
		if (result.hasErrors()) {
			modelAndView = new ModelAndView("item/salvar");
			return modelAndView;
		}
		
		ItemDAO itemDAO = ItemDAO.getInstance();
		
		try {
			itemDAO.cadastraItem(item);
		} catch (DAOException e) {
			// TODO Auto-generated catch block
			modelAndView = new ModelAndView("item/salvar");
			modelAndView.addObject("erro", "Erro ao salvar item : " + e.getLocalizedMessage());
			return modelAndView;
		}
		
		modelAndView = new ModelAndView("item/resumocadastro");
		modelAndView.addObject("resumo", "Item cadastrado com sucesso");
		
		return modelAndView;
	}

}
