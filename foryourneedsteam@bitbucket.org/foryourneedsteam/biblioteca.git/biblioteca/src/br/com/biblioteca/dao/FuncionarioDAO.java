package br.com.biblioteca.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.biblioteca.model.Funcionario;

public class FuncionarioDAO {
	
	private EntityManagerFactory factory ;
	private EntityManager manager;
	
	void cadastraFuncionario(Funcionario funcionario){
	
		try{
			
			factory = Persistence.createEntityManagerFactory("funcionario");
			manager = factory.createEntityManager();
			manager.getTransaction();
			manager.persist(funcionario);
			manager.getTransaction().commit();
			
		}
		catch (Exception e){
			
		}
		finally{
			manager.close();
		}
	}

	void pesquisaFuncionario (Funcionario funcionario){
		
		try{
			manager = factory.createEntityManager();
			Funcionario pesquisar = manager.find(Funcionario.class, funcionario.getId());
			
			
		}
		catch (Exception e){
			
		}
		finally{
			manager.close();
		}
		
	}

	void removeFuncionario (Funcionario funcionario){
		
		try{
			Funcionario encontrado = manager.find(Funcionario.class, funcionario.getId());
			manager.getTransaction().begin();
			manager.remove(encontrado);
			
		}
		catch (Exception e){
			
		}
		finally{
			manager.close();
		}
	}

	void atualizaFuncionario (Funcionario funcionario){
		
		try{
			manager.getTransaction().begin();
			manager.merge(funcionario);
			manager.getTransaction().commit();
		}
		catch(Exception e){
			
		}
		finally{
			manager.close();
		}
	}
}
