package br.com.biblioteca.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Solicitante {

	@Id // primary key
	@GeneratedValue(strategy = GenerationType.IDENTITY) // gerar ids automaticamente
	private Integer id;
	
	@NotNull // nao permitir campo nulo na aplicacao
	@Column(nullable = false) // nao permitir campo nulo na tabela
	private String nome;
	
	@NotNull // nao permitir campo nulo na aplicacao
	@Column(nullable = false, unique = true) // nao permitir campo nulo na tabela e valor unico para o campo
	private String ra;
	
	@NotNull // nao permitir campo nulo na aplicacao
	private char tipo;
	
	// relacionamento um para um  obrigatorio
	@OneToOne(optional = false, cascade = CascadeType.PERSIST)
	private Usuario usuario;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRa() {
		return ra;
	}

	public void setRa(String ra) {
		this.ra = ra;
	}

	public char getTipo() {
		return tipo;
	}

	public void setTipo(char tipo) {
		this.tipo = tipo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
