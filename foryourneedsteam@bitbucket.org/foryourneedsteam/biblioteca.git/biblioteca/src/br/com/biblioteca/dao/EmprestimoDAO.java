package br.com.biblioteca.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import br.com.biblioteca.model.Emprestimo;
import br.com.biblioteca.persistence.util.PersistenceUtil;

public class EmprestimoDAO {

	private EntityManagerFactory factory;
	private EntityManager manager;
	private static EmprestimoDAO emprestimoDAO;

	private EmprestimoDAO() {
		// TODO Auto-generated constructor stub
		factory = PersistenceUtil.getInstance();
	}

	public static EmprestimoDAO getInstance() {
		if (emprestimoDAO == null) {
			emprestimoDAO = new EmprestimoDAO();
		}
		return emprestimoDAO;
	}

	void cadastraEmprestimo(Emprestimo emprestimo) {
		try {			
			manager = factory.createEntityManager();
			manager.getTransaction();
			manager.persist(emprestimo);
			manager.getTransaction().commit();

		} catch (Exception e) {

		} finally {
			manager.close();
		}
	}
}
