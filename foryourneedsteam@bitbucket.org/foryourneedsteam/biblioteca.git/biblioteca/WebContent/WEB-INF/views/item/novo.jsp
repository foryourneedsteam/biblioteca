<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<template:templateusuario title="Salvar Item">
	<jsp:body>
		<form:form servletRelativeAction="/item/salvar" method="post" modelAttribute="item">
			<form:errors />
			
			<table>
				<tr>
					<th>
						<form:label path="item.descricao">Descri��o</form:label>						
					</th>
					<td>
						<form:input path="descricao" size="25" />
					</td>
				</tr>
				<tr>
					<th>
						<form:label path="tipoAquisicao">Compra</form:label>						
					</th>
					<td>
						<form:radiobutton path="tipoAquisicao" value="C" /> Doa��o
						<form:radiobutton path="tipoAquisicao" value="D" /> Compra
					</td>
				</tr>
				<tr>
					<th>
						<form:label path="valorCompra">Valor Compra</form:label>						
					</th>
					<td>
						<form:input path="valorCompra" />
					</td>
				</tr>
				<tr>
					<th>
						<form:label path="edEspecial">Edi��o Especial</form:label>						
					</th>
					<td>
						<form:checkbox path="edEspecial"/>Sim
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="submit" value="Salvar" />
					</td>
				</tr>
			</table>
		</form:form>
	</jsp:body>	
</template:templateusuario>