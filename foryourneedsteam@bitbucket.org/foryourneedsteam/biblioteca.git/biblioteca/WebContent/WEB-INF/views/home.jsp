<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<template:templateusuario title="Biblioteca">
	<jsp:body>
		<h3>P�gina Inicial</h3>
		
		<c:url value="/item/novo" var="salvaritem" />
		
		<a href="${salvaritem }">Salvar item</a>
	</jsp:body>	
</template:templateusuario>