<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ attribute name="title" required="true" %>

<!DOCTYPE html />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Biblioteca | ${title }</title>
</head>
<body>
	<h1>
		${title }
	</h1>
	
	<jsp:doBody />
	
	<hr />
	<h1>
		${footer }
	</h1>
</body>
</html>